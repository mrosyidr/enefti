# Enefti App

A Simple App for Viewing and Saving Images from the Art Institute of Chicago API, as part of Rosyid's submission in FrontEnd Technical Test.

# Technology Stack

Flutter SDK 2.10.0

## Supported Version

1.  Current Platform: Android OS
2.  Devices Tested: OnePlus 5T, Nexus S (Portrait mode)

## Dependencies
1.  [HTTP](https://pub.dev/packages/http) for remote client API handling
2.  [Cached Network Image](https://pub.dev/packages/cached_network_image) for image handling
3.  [Hive and Hive Flutter](https://pub.dev/packages/hive) for local storage handling
4.  [Infinite Scroll Pagination](https://pub.dev/packages/infinite_scroll_pagination) for pagination handling
5.  and other dependencies like build_runner and hive_generator.
6.  additional dependencies for no_image handling, using [Wikimedia's No Picture](https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png)

## Local Development
Here are some useful dart/flutter commands for executing this project:

* `flutter clean && dart pub get && flutter pub get && flutter pub run build_runner build --delete-conflicting-outputs` - clean and get packages, then run generated config
* `flutter run` - Build the entire project

# User Guide
1.  [Explore Images](https://drive.google.com/file/d/1L2cietPPPNIE1v5DWtoJf-VHekiEQvC3/view?usp=sharing) - explore any beautiful and artistict images in home page. 
2.  [Search by Keyword](https://drive.google.com/file/d/1Myg6QToRUBkNmrvd-VpIUwexDrukrw7l/view?usp=sharing) - type your keyword into search button in home page.
3.  [Add to Favorites](https://drive.google.com/file/d/1C_-6OeLP9Cp-Db2TBuvNs3cNhiMj9b5v/view?usp=sharing) - save your favorite images by tap-on favorite icon in each image detail page.
4.  [Remove from Favorites](https://drive.google.com/file/d/1guFk-OZy_p5xEqcOfpgNucPlFjkcAQJq/view?usp=sharing) - and you can remove your favorite image from the list by either untap-on favorite icon or tap remove icon in favorites page.

# Next Development Plan
1.  Use [Clean Architecture](https://github.com/android10/Android-CleanArchitecture)
2.  Adding some features to make this project more useful.


# Download APK
Please [find it here](https://drive.google.com/file/d/1qkB_36HhxZNmbs0ABwDup5UzBvUAEzmb/view?usp=sharing) if you want to try in your devices.
