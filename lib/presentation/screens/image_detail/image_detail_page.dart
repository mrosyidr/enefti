import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:enefti/model/image_summary.dart';
import 'package:enefti/model/saved_image.dart';
import 'package:enefti/presentation/common/enefti_text.dart';
import 'package:enefti/presentation/common/themes/colors.dart';
import 'package:enefti/util/boxes.dart';
import 'package:enefti/util/manage_image/add_to_favorites.dart';
import 'package:enefti/util/manage_image/remove_from_favorites.dart';

class ImageDetailPage extends StatefulWidget {
  final ImageSummary imageItem;
  const ImageDetailPage({Key? key, required this.imageItem}) : super(key: key);

  @override
  _ImageDetailPageState createState() => _ImageDetailPageState();
}

class _ImageDetailPageState extends State<ImageDetailPage> {
  bool isFavorite = false;
  Box<SavedImage> savedImageBox = Boxes.getSavedImage();
  SavedImage savedImage = SavedImage();
  late bool foundImage;

  @override
  void initState() {
    foundImage = savedImageBox.values.any(
      (element) {
        savedImage = element;
        return element.id == widget.imageItem.id;
      },
    );

    if (foundImage == true) {
      isFavorite = true;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: EneftiText.heading(text: widget.imageItem.title),
      ),
      body: ValueListenableBuilder<Box<SavedImage>>(
        valueListenable: savedImageBox.listenable(),
        builder: (_, box, __) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  CachedNetworkImage(
                    imageUrl: widget.imageItem.imageUrl,
                  ),
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                child: EneftiText.bodyItalic(
                                  text:
                                      'Credit: ${widget.imageItem.creditLine}',
                                  maxLines: 3,
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            IconButton(
                              icon: isFavorite
                                  ? const Icon(
                                      Icons.favorite,
                                      color: primaryColor,
                                    )
                                  : const Icon(Icons.favorite_outline),
                              onPressed: () {
                                setState(() {
                                  if (isFavorite == false) {
                                    isFavorite = true;
                                    addToFavorites(
                                      widget.imageItem.id,
                                      widget.imageItem.title,
                                      widget.imageItem.imageId,
                                      widget.imageItem.imageUrl,
                                      widget.imageItem.inscriptions,
                                      widget.imageItem.provenanceText,
                                      widget.imageItem.publicationHistory,
                                      widget.imageItem.exhibitionHistory,
                                      widget.imageItem.creditLine,
                                      isFavorite,
                                    );
                                  } else {
                                    removeFromFavorites(savedImage);
                                    isFavorite = false;
                                  }
                                });
                              },
                            ),
                          ],
                        ),
                        const SizedBox(height: 16),
                        EneftiText.heading(
                          text: 'Art Title (ID: ${widget.imageItem.id})',
                        ),
                        const SizedBox(height: 8),
                        EneftiText.bodyNormal(
                          text: widget.imageItem.title != ''
                              ? widget.imageItem.title
                              : '-',
                        ),
                        const SizedBox(height: 16),
                        const EneftiText.heading(text: 'Inscription'),
                        const SizedBox(height: 8),
                        EneftiText.bodyNormal(
                          text: widget.imageItem.inscriptions != ''
                              ? widget.imageItem.inscriptions
                              : '-',
                        ),
                        const SizedBox(height: 16),
                        const EneftiText.heading(text: 'Provenance Text'),
                        const SizedBox(height: 8),
                        EneftiText.bodyNormal(
                          text: widget.imageItem.provenanceText != ''
                              ? widget.imageItem.provenanceText
                              : '-',
                        ),
                        const SizedBox(height: 16),
                        const EneftiText.heading(text: 'Publication History'),
                        const SizedBox(height: 8),
                        EneftiText.bodyNormal(
                          text: widget.imageItem.publicationHistory != ''
                              ? widget.imageItem.publicationHistory
                              : '-',
                        ),
                        const SizedBox(height: 16),
                        const EneftiText.heading(text: 'Exhibition History'),
                        const SizedBox(height: 8),
                        EneftiText.bodyNormal(
                          text: widget.imageItem.exhibitionHistory != ''
                              ? widget.imageItem.exhibitionHistory
                              : '-',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
