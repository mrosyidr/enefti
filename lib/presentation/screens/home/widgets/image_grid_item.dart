import 'package:enefti/model/saved_image.dart';
import 'package:enefti/presentation/common/themes/colors.dart';
import 'package:enefti/util/boxes.dart';
import 'package:flutter/cupertino.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:enefti/model/image_summary.dart';
import 'package:enefti/presentation/screens/image_detail/image_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class ImageGridItem extends StatelessWidget {
  final ImageSummary imageItem;
  const ImageGridItem({required this.imageItem, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Box<SavedImage> savedImageBox = Boxes.getSavedImage();
    bool foundImage = savedImageBox.values.any(
      (element) {
        return element.id == imageItem.id;
      },
    );
    bool isFavorite = foundImage ? true : false;

    final Image noImage = Image.network(
        'https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png');

    return ValueListenableBuilder<Box<SavedImage>>(
        valueListenable: savedImageBox.listenable(),
        builder: (_, box, __) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => ImageDetailPage(
                    imageItem: imageItem,
                  ),
                ),
              );
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(
                  color: isFavorite ? primaryColor : Colors.transparent,
                  width: isFavorite ? 2 : 1,
                ),
                boxShadow: const [
                  BoxShadow(
                    color: boxShadow1,
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: Offset(0, 2),
                  ),
                  BoxShadow(
                    color: boxShadow2,
                    spreadRadius: 0,
                    blurRadius: 2,
                    offset: Offset(0, 0),
                  ),
                ],
              ),
              child: CachedNetworkImage(
                imageUrl: imageItem.imageUrl,
                errorWidget: (_, error, __) {
                  return noImage;
                },
              ),
            ),
          );
        }
        );
  }
}
