import 'dart:async';

import 'package:enefti/model/image_summary.dart';
import 'package:enefti/model/saved_image.dart';
import 'package:enefti/presentation/common/image_search_input_sliver.dart';
import 'package:enefti/presentation/screens/home/bloc/image_grid_bloc.dart';
import 'package:enefti/presentation/screens/home/widgets/image_grid_item.dart';
import 'package:enefti/util/boxes.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class ImageGridView extends StatefulWidget {
  const ImageGridView({Key? key}) : super(key: key);

  @override
  _ImageGridViewState createState() => _ImageGridViewState();
}

class _ImageGridViewState extends State<ImageGridView> {
  final ImageGridBloc _bloc = ImageGridBloc();
  final PagingController<int, ImageSummary> _pagingController =
      PagingController(firstPageKey: 0);
  late StreamSubscription _blocListingStateSubscription;
  Box<SavedImage> savedImageBox = Boxes.getSavedImage();

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _bloc.onPageRequestSink.add(pageKey);
    });

    _blocListingStateSubscription = _bloc.onNewListingState.listen(
      (listingState) {
        _pagingController.value = PagingState(
          nextPageKey: listingState.nextPageKey,
          error: listingState.error,
          itemList: listingState.itemList,
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<SavedImage>>(
        valueListenable: savedImageBox.listenable(),
        builder: (_, box, __) {
          return CustomScrollView(
            slivers: <Widget>[
              ImageSearchInputSliver(
                onChanged: (searchTerm) => _bloc.onSearchInputChangedSink.add(
                  searchTerm,
                ),
              ),
              PagedSliverGrid<int, ImageSummary>(
                pagingController: _pagingController,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 100 / 150,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  crossAxisCount: 3,
                ),
                builderDelegate: PagedChildBuilderDelegate<ImageSummary>(
                  itemBuilder: (context, item, index) => ImageGridItem(
                    imageItem: item,
                  ),
                ),
              ),
            ],
          );
        }
        );
  }

  @override
  void dispose() {
    _pagingController.dispose();
    _blocListingStateSubscription.cancel();
    _bloc.dispose();
    super.dispose();
  }
}
