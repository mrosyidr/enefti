import 'package:enefti/model/image_summary.dart';

class ImageGridStates {
  ImageGridStates({
    this.itemList,
    this.error,
    this.nextPageKey = 0,
  });

  final List<ImageSummary>? itemList;
  final dynamic error;
  final int? nextPageKey;
}
