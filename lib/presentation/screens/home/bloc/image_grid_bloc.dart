import 'dart:async';

import 'package:enefti/presentation/screens/home/bloc/image_grid_states.dart';
import 'package:enefti/util/remote_api.dart';
import 'package:rxdart/rxdart.dart';

class ImageGridBloc {
  ImageGridBloc() {
    _onPageRequest.stream
        .flatMap(_fetchImageSummaryList)
        .listen(_onNewListingStateController.add)
        .addTo(_subscriptions);

    _onSearchInputChangedSubject.stream
        .flatMap((_) => _resetSearch())
        .listen(_onNewListingStateController.add)
        .addTo(_subscriptions);
  }

  static const _pageSize = 15; // as per required in document

  final _subscriptions = CompositeSubscription();

  final _onNewListingStateController =
      BehaviorSubject<ImageGridStates>.seeded(
    ImageGridStates(),
  );

  Stream<ImageGridStates> get onNewListingState =>
      _onNewListingStateController.stream;

  final _onPageRequest = StreamController<int>();

  Sink<int> get onPageRequestSink => _onPageRequest.sink;

  final _onSearchInputChangedSubject = BehaviorSubject<String?>.seeded(null);

  Sink<String?> get onSearchInputChangedSink =>
      _onSearchInputChangedSubject.sink;

  String? get _searchInputValue => _onSearchInputChangedSubject.value;

  Stream<ImageGridStates> _resetSearch() async* {
    yield ImageGridStates();
    yield* _fetchImageSummaryList(0);
  }

  Stream<ImageGridStates> _fetchImageSummaryList(int pageKey) async* {
    final lastListingState = _onNewListingStateController.value;
    try {
      final newItems = await RemoteApi.getImageList(
        pageKey,
        _pageSize,
        searchTerm: _searchInputValue,
      );

      final isLastPage = newItems.length < _pageSize;
      final nextPageKey = isLastPage ? null : pageKey + newItems.length;
      yield ImageGridStates(
        error: null,
        nextPageKey: nextPageKey,
        itemList: [...lastListingState.itemList ?? [], ...newItems],
      );
    } catch (e) {
      yield ImageGridStates(
        error: e,
        nextPageKey: lastListingState.nextPageKey,
        itemList: lastListingState.itemList,
      );
    }
  }

  void dispose() {
    _onSearchInputChangedSubject.close();
    _onNewListingStateController.close();
    _subscriptions.dispose();
    _onPageRequest.close();
  }
}
