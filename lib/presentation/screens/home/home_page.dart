import 'package:enefti/presentation/common/themes/colors.dart';
import 'package:enefti/presentation/screens/home/widgets/image_grid_view.dart';
import 'package:enefti/presentation/screens/saved_images/saved_images_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int pageIndex = 0;
  List<Widget> pages = [
    const ImageGridView(),
    const SavedImagesPage(),
  ];

  @override
  void dispose() {
    super.dispose();
    pages;
    pageIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: _buildBody(),
      bottomNavigationBar: _buildNavBar(),
    );
  }

  Widget _buildBody() {
    return SafeArea(
      child: IndexedStack(
        index: pageIndex,
        children: pages,
      ),
    );
  }

  Widget _buildNavBar() {
    return BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
        BottomNavigationBarItem(icon: Icon(Icons.favorite), label: 'Favorites'),
      ],
      currentIndex: pageIndex,
      selectedItemColor: primaryColor,
      onTap: selectedTab,
    );
  }

  selectedTab(index) {
    setState(() {
      pageIndex = index;
    });
  }
}
