import 'package:cached_network_image/cached_network_image.dart';
import 'package:enefti/model/image_summary.dart';
import 'package:enefti/model/saved_image.dart';
import 'package:enefti/presentation/common/confirmation_modal.dart';
import 'package:enefti/presentation/common/enefti_text.dart';
import 'package:enefti/presentation/common/themes/colors.dart';
import 'package:enefti/presentation/screens/image_detail/image_detail_page.dart';
import 'package:enefti/presentation/screens/saved_images/widgets/saved_images_empty_page.dart';
import 'package:enefti/util/boxes.dart';
import 'package:enefti/util/manage_image/remove_from_favorites.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

class SavedImagesPage extends StatefulWidget {
  const SavedImagesPage({Key? key}) : super(key: key);

  @override
  _SavedImagesPageState createState() => _SavedImagesPageState();
}

class _SavedImagesPageState extends State<SavedImagesPage> {
  Box<SavedImage> savedImageBox = Boxes.getSavedImage();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const EneftiText.heading(text: 'My Favorite Arts'),
      ),
      body: ValueListenableBuilder<Box<SavedImage>>(
        valueListenable: savedImageBox.listenable(),
        builder: (_, box, __) {
          return SafeArea(
            child: CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: [
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      SingleChildScrollView(
                        child: savedImageBox.values.isNotEmpty
                            ? Column(
                                children: List.generate(
                                  savedImageBox.values.length,
                                  (index) {
                                    ImageSummary currImageItem = ImageSummary(
                                      id: savedImageBox.values
                                          .elementAt(index)
                                          .id,
                                      title: savedImageBox.values
                                          .elementAt(index)
                                          .title,
                                      imageId: savedImageBox.values
                                          .elementAt(index)
                                          .imageId,
                                      imageUrl: savedImageBox.values
                                          .elementAt(index)
                                          .imageUrl,
                                      inscriptions: savedImageBox.values
                                          .elementAt(index)
                                          .inscriptions,
                                      provenanceText: savedImageBox.values
                                          .elementAt(index)
                                          .provenanceText,
                                      publicationHistory: savedImageBox.values
                                          .elementAt(index)
                                          .publicationHistory,
                                      exhibitionHistory: savedImageBox.values
                                          .elementAt(index)
                                          .exhibitionHistory,
                                      creditLine: savedImageBox.values
                                          .elementAt(index)
                                          .creditLine,
                                    );
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          CupertinoPageRoute(
                                            builder: (context) =>
                                                ImageDetailPage(
                                              imageItem: currImageItem,
                                            ),
                                          ),
                                        );
                                      },
                                      child: Card(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 8),
                                        elevation: 1.5,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(16.0),
                                        ),
                                        child: Column(
                                          children: [
                                            CachedNetworkImage(
                                              imageUrl: savedImageBox.values
                                                  .elementAt(index)
                                                  .imageUrl,
                                            ),
                                            Container(
                                              padding: const EdgeInsets.all(16),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Flexible(
                                                        child: Column(
                                                          children: [
                                                            EneftiText.heading(
                                                              text: savedImageBox
                                                                  .values
                                                                  .elementAt(
                                                                      index)
                                                                  .title,
                                                              maxLines: 2,
                                                            ),
                                                            EneftiText
                                                                .bodyNormal(
                                                              text: savedImageBox
                                                                          .values
                                                                          .elementAt(
                                                                              index)
                                                                          .inscriptions !=
                                                                      ''
                                                                  ? savedImageBox
                                                                      .values
                                                                      .elementAt(
                                                                          index)
                                                                      .inscriptions
                                                                  : '(No Inscription Text)',
                                                              maxLines: 5,
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      const SizedBox(width: 16),
                                                      IconButton(
                                                        onPressed: () {
                                                          showDialog(
                                                            context: context,
                                                            builder:
                                                                (BuildContext
                                                                    context) {
                                                              return ConfirmationModal(
                                                                title:
                                                                    'Remove from Favorites',
                                                                description:
                                                                    'Are you sure to take this image out from your favorites?',
                                                                negativeText:
                                                                    'Cancel',
                                                                positiveText:
                                                                    'YES',
                                                                onPressedNegative:
                                                                    () {
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                onPressedPositive:
                                                                    () {
                                                                  setState(() {
                                                                    removeFromFavorites(
                                                                      savedImageBox
                                                                          .values
                                                                          .elementAt(
                                                                              index),
                                                                    );
                                                                    Navigator.pop(
                                                                        context);
                                                                  });
                                                                },
                                                              );
                                                            },
                                                          );
                                                        },
                                                        icon: const Icon(
                                                          Icons.delete,
                                                          color: negativeColor,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              )
                            : const SavedImagesEmptyPage(),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
