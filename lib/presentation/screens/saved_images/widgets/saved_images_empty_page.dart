import 'package:flutter/material.dart';

import 'package:enefti/presentation/common/enefti_text.dart';

class SavedImagesEmptyPage extends StatelessWidget {
  const SavedImagesEmptyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 32),
      child: Column(
        children: const [
          EneftiText.heading(
            text: 'No Items Found',
            alignment: Alignment.center,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 8),
          EneftiText.bodyNormal(
            text: 'The list is currently empty.',
            alignment: Alignment.center,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
