import 'package:enefti/presentation/common/themes/typography.dart';
import 'package:flutter/material.dart';

class EneftiText extends StatelessWidget {
  final String text;
  final TextStyle textStyle;
  final TextAlign? textAlign;
  final Alignment? alignment;
  final TextOverflow? overflow;
  final int? maxLines;
  final EdgeInsets? margin;
  final Color? color;

  const EneftiText.heading(
      {Key? key,
      required this.text,
      this.textStyle = headingBold,
      this.textAlign = TextAlign.left,
      this.alignment = Alignment.centerLeft,
      this.overflow = TextOverflow.ellipsis,
      this.maxLines,
      this.margin,
      this.color})
      : super(key: key);

  const EneftiText.bodyNormal(
      {Key? key,
      required this.text,
      this.textStyle = bodyNormal,
      this.textAlign = TextAlign.left,
      this.alignment = Alignment.centerLeft,
      this.overflow = TextOverflow.ellipsis,
      this.maxLines,
      this.margin,
      this.color})
      : super(key: key);

  const EneftiText.bodyItalic(
      {Key? key,
      required this.text,
      this.textStyle = bodyItalic,
      this.textAlign = TextAlign.left,
      this.alignment = Alignment.centerLeft,
      this.overflow = TextOverflow.ellipsis,
      this.maxLines,
      this.margin,
      this.color})
      : super(key: key);

  const EneftiText.bodySmall(
      {Key? key,
      required this.text,
      this.textStyle = bodySmall,
      this.textAlign = TextAlign.left,
      this.alignment = Alignment.centerLeft,
      this.overflow = TextOverflow.ellipsis,
      this.maxLines,
      this.margin,
      this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      margin: margin,
      child: Text(
        text,
        style: textStyle.copyWith(color: color),
        textAlign: textAlign,
        maxLines: maxLines,
        overflow: overflow,
      ),
    );
  }
}
