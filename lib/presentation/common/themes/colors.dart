import 'package:flutter/material.dart';

const primaryColor = Colors.teal;
const secondaryColor = Color.fromARGB(255, 5, 102, 92);
const boxShadow1 = Color.fromRGBO(51, 173, 92, 0.1);
const boxShadow2 = Color.fromRGBO(51, 173, 92, 0.2);
const negativeColor = Color.fromARGB(255, 168, 32, 22);
const gradientColor = [
  Color.fromARGB(255, 51, 150, 140),
  secondaryColor,
  primaryColor
];