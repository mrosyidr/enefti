import 'package:flutter/material.dart';

const TextStyle headingBold = TextStyle(
  decoration: TextDecoration.none,
  fontFamily: 'Telkomsel Batik',
  fontWeight: FontWeight.bold,
  fontStyle: FontStyle.normal,
  fontSize: 16
);

const TextStyle bodyNormal = TextStyle(
  decoration: TextDecoration.none,
  fontFamily: 'Telkomsel Batik',
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
  fontSize: 14
);

const TextStyle bodySmall = TextStyle(
  decoration: TextDecoration.none,
  fontFamily: 'Telkomsel Batik',
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.normal,
  fontSize: 12
);

const TextStyle bodyItalic = TextStyle(
  decoration: TextDecoration.none,
  fontFamily: 'Telkomsel Batik',
  fontWeight: FontWeight.normal,
  fontStyle: FontStyle.italic,
  fontSize: 14
);
