import 'package:enefti/presentation/common/enefti_button.dart';
import 'package:enefti/presentation/common/enefti_text.dart';
import 'package:flutter/material.dart';

class ConfirmationModal extends StatelessWidget {
  final String title;
  final String description;
  final String negativeText;
  final Function onPressedNegative;
  final String positiveText;
  final Function onPressedPositive;

  const ConfirmationModal({
    Key? key,
    required this.title,
    required this.description,
    required this.negativeText,
    required this.onPressedNegative,
    required this.positiveText,
    required this.onPressedPositive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            EneftiText.heading(
              text: title,
            ),
            const SizedBox(height: 16),
            EneftiText.bodyNormal(
              text: description,
              maxLines: 2,
            ),
            const SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: EneftiButton(
                    onPressed: onPressedNegative,
                    text: negativeText,
                    type: EneftiButtonType.bordered,
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: EneftiButton(
                    onPressed: onPressedPositive,
                    text: positiveText,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
