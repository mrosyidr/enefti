import 'package:enefti/presentation/common/enefti_text.dart';
import 'package:flutter/material.dart';
import 'package:enefti/presentation/common/themes/colors.dart';

enum EneftiButtonType { gradient, bordered }

enum EneftiButtonSize { medium, small }

class EneftiButton extends StatelessWidget {
  final String text;
  final Function? onPressed;
  final bool disabled;
  final bool isLoading;
  final EneftiButtonType type;
  final EneftiButtonSize size;
  final Color color;
  final ImageProvider? prefixIcon;
  final ImageProvider? suffixIcon;

  const EneftiButton({
    Key? key,
    this.text = '',
    this.onPressed,
    this.disabled = false,
    this.isLoading = false,
    this.type = EneftiButtonType.gradient,
    this.size = EneftiButtonSize.medium,
    this.color = primaryColor,
    this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = 44;
    if (size == EneftiButtonSize.small) height = 22;

    double iconSize = 20;
    if (size == EneftiButtonSize.small) iconSize = 14;

    return Container(
      height: height,
      decoration: ShapeDecoration(
        shape: StadiumBorder(
          side: BorderSide(
            color: type == EneftiButtonType.bordered ? color : Colors.transparent,
          ),
        ),
        gradient: LinearGradient(
          colors: type == EneftiButtonType.bordered
              ? [Colors.transparent, Colors.transparent]
              : disabled
                  ? [secondaryColor, primaryColor]
                  : gradientColor,
          begin: Alignment.centerRight,
          end: Alignment.centerLeft,
        ),
      ),
      child: MaterialButton(
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        shape: const StadiumBorder(),
        onPressed: isLoading || disabled
            ? null
            : () {
                if (!isLoading && !disabled) onPressed?.call();
              },
        child: isLoading
            ? _buildLoading()
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  prefixIcon != null
                      ? ImageIcon(
                          prefixIcon,
                          size: iconSize,
                          color: color,
                        )
                      : Container(),
                  prefixIcon != null ? const SizedBox(width: 10) : Container(),
                  _buildSize(),
                  suffixIcon != null ? const SizedBox(width: 10) : Container(),
                  suffixIcon != null
                      ? ImageIcon(suffixIcon, size: 20)
                      : Container(),
                ],
              ),
      ),
    );
  }

  Widget _buildSize() {
    var button = EneftiText.bodyNormal(
      text: text,
      color: type == EneftiButtonType.bordered ? color : Colors.white,
      alignment: Alignment.center,
    );
    if (size == EneftiButtonSize.small) {
      button = EneftiText.bodySmall(
        text: text,
        color: type == EneftiButtonType.bordered ? color : secondaryColor,
        alignment: Alignment.center,
      );
    }
    return button;
  }

  Widget _buildLoading() {
    return const CircularProgressIndicator(
      color: primaryColor,
    );
  }
}
