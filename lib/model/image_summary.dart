/// Summarized information of an ARTIC Image.
class ImageSummary {
  ImageSummary({
    required this.id,
    required this.title,
    required this.imageId,
    required this.imageUrl,
    required this.inscriptions,
    required this.provenanceText,
    required this.publicationHistory,
    required this.exhibitionHistory,
    required this.creditLine,
  });

  factory ImageSummary.fromJson(Map<String, dynamic> json) {
    String _identifier = json['image_id'] ?? '';
    String _imgUrl = _identifier != ''
        ? "https://www.artic.edu/iiif/2/$_identifier/full/843,/0/default.jpg"
        : 'https://upload.wikimedia.org/wikipedia/commons/f/fc/No_picture_available.png';

    return ImageSummary(
      id: json['id'],
      title: json['title'] ?? 'Untitled',
      imageId: _identifier,
      imageUrl: _imgUrl,
      inscriptions: json['inscriptions'] ?? '',
      provenanceText: json['provenance_text'] ?? '',
      publicationHistory: json['publication_history'] ?? '',
      exhibitionHistory: json['exhibition_history'] ?? '',
      creditLine: json['credit_line'] ?? '',
    );
  }

  final int id;
  final String title;
  final String imageId;
  final String imageUrl;
  final String inscriptions;
  final String provenanceText;
  final String publicationHistory;
  final String exhibitionHistory;
  final String creditLine;
}
