import 'package:hive/hive.dart';

part 'saved_image.g.dart';

@HiveType(typeId: 0)
class SavedImage extends HiveObject {
  @HiveField(0)
  late int id;

  @HiveField(1)
  late String title;

  @HiveField(2)
  late String imageId;

  @HiveField(3)
  late String imageUrl;

  @HiveField(4)
  late String inscriptions;

  @HiveField(5)
  late String provenanceText;

  @HiveField(6)
  late String publicationHistory;

  @HiveField(7)
  late String exhibitionHistory;

  @HiveField(8)
  late String creditLine;

  @HiveField(9)
  bool isSaved = false;
}
