// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_image.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SavedImageAdapter extends TypeAdapter<SavedImage> {
  @override
  final int typeId = 0;

  @override
  SavedImage read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SavedImage()
      ..id = fields[0] as int
      ..title = fields[1] as String
      ..imageId = fields[2] as String
      ..imageUrl = fields[3] as String
      ..inscriptions = fields[4] as String
      ..provenanceText = fields[5] as String
      ..publicationHistory = fields[6] as String
      ..exhibitionHistory = fields[7] as String
      ..creditLine = fields[8] as String
      ..isSaved = fields[9] as bool;
  }

  @override
  void write(BinaryWriter writer, SavedImage obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.imageId)
      ..writeByte(3)
      ..write(obj.imageUrl)
      ..writeByte(4)
      ..write(obj.inscriptions)
      ..writeByte(5)
      ..write(obj.provenanceText)
      ..writeByte(6)
      ..write(obj.publicationHistory)
      ..writeByte(7)
      ..write(obj.exhibitionHistory)
      ..writeByte(8)
      ..write(obj.creditLine)
      ..writeByte(9)
      ..write(obj.isSaved);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SavedImageAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
