import 'package:hive/hive.dart';

import 'package:enefti/model/saved_image.dart';

class Boxes {
  static Box<SavedImage> getSavedImage() => Hive.box<SavedImage>('savedImage');
}
