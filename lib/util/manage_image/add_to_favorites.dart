import 'package:enefti/model/saved_image.dart';
import 'package:enefti/util/boxes.dart';

Future addToFavorites(
    int id,
    String title,
    String imageId,
    String imageUrl,
    String inscriptions,
    String provenanceText,
    String publicationHistory,
    String exhibitionHistory,
    String creditLine,
    bool isSaved,
  ) async {
    final savedImage = SavedImage()
      ..id = id
      ..title = title
      ..imageId = imageId
      ..imageUrl = imageUrl
      ..inscriptions = inscriptions
      ..provenanceText = provenanceText
      ..publicationHistory = publicationHistory
      ..exhibitionHistory = exhibitionHistory
      ..creditLine = creditLine
      ..isSaved = isSaved;

    final box = Boxes.getSavedImage();
    box.put(id, savedImage);
  }