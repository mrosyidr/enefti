import 'package:enefti/model/saved_image.dart';

void removeFromFavorites(SavedImage savedImage) {
  savedImage.delete();
}
