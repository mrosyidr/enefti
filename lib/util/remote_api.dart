import 'dart:convert';
import 'dart:io';

import 'package:enefti/model/image_summary.dart';
import 'package:http/http.dart' as http;

class RemoteApi {
  static Future<List<ImageSummary>> getImageList(
    int offset,
    int limit, {
    String? searchTerm,
  }) async {
    return http
        .get(_ApiUrlBuilder.imageList(offset, limit, searchTerm: searchTerm))
        .mapFromResponse<List<ImageSummary>, List<dynamic>>((jsonArray) {
      return _parseItemListFromJsonArray(jsonArray, (jsonObject) {
        return ImageSummary.fromJson(jsonObject);
      });
    });
  }

  static List<T> _parseItemListFromJsonArray<T>(
    List<dynamic> jsonArray,
    T Function(dynamic object) mapper,
  ) {
    return jsonArray.map(mapper).toList();
  }
}

class GenericHttpException implements Exception {}

class NoConnectionException implements Exception {}

class _ApiUrlBuilder {
  static const _baseUrl = 'https://api.artic.edu/api/v1/artworks/search';

  static Uri imageList(
    int offset,
    int limit, {
    String? searchTerm,
  }) {
    searchTerm = searchTerm ?? '';
    return Uri.parse('$_baseUrl?'
        'page=$offset'
        '&limit=$limit'
        '&q=$searchTerm');
  }
}

extension on Future<http.Response> {
  Future<R> mapFromResponse<R, T>(R Function(T) jsonParser) async {
    try {
      final response = await this;
      if (response.statusCode == 200) {
        List<dynamic> ids = [];

        for (int i = 0; i < jsonDecode(response.body)['data'].length; i++) {
          ids.add(jsonDecode(response.body)['data'][i]['id']);
        }

        String idsParam = ids.isNotEmpty ? ids.join(',') : '';

        try {
          String finalApiURL =
              'https://api.artic.edu/api/v1/artworks?ids=$idsParam&fields=id,title,image_id,inscriptions,provenance_text,publication_history,exhibition_history,credit_line';
          final responseFinal = await http.get(Uri.parse(finalApiURL));
          if (responseFinal.statusCode == 200) {
            return jsonParser(jsonDecode(responseFinal.body)['data']);
          }
        } on SocketException {
          throw NoConnectionException();
        }

        return jsonParser(jsonDecode(response.body)['data']);
      } else {
        throw GenericHttpException();
      }
    } on SocketException {
      throw NoConnectionException();
    }
  }
}
