import 'package:enefti/presentation/common/themes/colors.dart';
import 'package:flutter/material.dart';

import 'package:hive_flutter/hive_flutter.dart';

import 'package:enefti/model/saved_image.dart';
import 'package:enefti/presentation/screens/home/home_page.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Hive.initFlutter();
  Hive.registerAdapter(SavedImageAdapter());
  await Hive.openBox<SavedImage>('savedImage');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Enefti',
      theme: ThemeData(
        primarySwatch: primaryColor,
      ),
      home: const HomePage(),
    );
  }
}
